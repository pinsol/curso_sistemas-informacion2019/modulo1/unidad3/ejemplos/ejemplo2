﻿                                              /** Modulo I - Unidad 3 - Hoja Ejemplos 2 **/
DROP DATABASE IF ejemploprogramacion2;
CREATE DATABASE IF NOT EXISTS ejemploprogramacion2;
USE ejemploprogramacion2;
         
/* 
  Ejemplo 1-
  Procedimiento almacenado que reciba un texto y un carácter. Debe indicarte
  si ese carácter está en el texto. Realizar con: LOCATE, POSITION
*/
                   
-- con LOCATE
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1a(frase varchar(150), caracter char(1))
  BEGIN
    -- declara variable
    DECLARE resultado varchar(50) DEFAULT 'El caracter no esta en la frase';

    -- crea la operacion    
    IF (LOCATE(caracter,frase)<>0) THEN
    set resultado = 'El caracter esta';
    END IF;
    
    -- muestra el resultado
    SELECT resultado;
  END //
DELIMITER ;

CALL ej1a('Esto es un ejemplo','n');

-- con POSITION
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1b(frase varchar(150), caracter char(1))
  BEGIN
    IF (POSITION(caracter IN frase)>0) THEN 
      SELECT 'El caracter esta';
    ELSE 
      SELECT 'El caracter no esta en la frase';
    END IF;
  END //
DELIMITER ;

CALL ej1b('Esto es un ejemplo','n');


/* 
  Ejemplo 2-
  Procedimiento almacenado que reciba un texto y un caracter que debe indicar
  el texto completo que haya antes de la primera vez que aparece ese caracter
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2(texto varchar(250), caracter1 char(1))
  BEGIN
    DECLARE resultado varchar(250) DEFAULT NULL;

    -- buscamos el caracter y leemos el texto
    SET resultado = SUBSTRING_INDEX(texto,caracter1,1);

    -- mostramos el resultado
    SELECT resultado;
  END //
DELIMITER ;

CALL ej2('esto es un ejemplo','n');

/*
  Ejemplo 3-
  Procedimiento almacenado que reciba tres números y dos argumentos de
  tipo salida donde devuelva el número más grande y el número más pequeño de los tres
  números pasados
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej3(IN num1 int, IN num2 int, IN num3 int, OUT arg1 int, OUT arg2 int)
  BEGIN
    set arg1=GREATEST(num1,num2,num3);
    set arg2=LEAST(num1,num2,num3);
  END //
DELIMITER ;

CALL ej3(1,2,3,@arg1,@arg2);

SELECT @arg1, @arg2;

/*
  Ejemplo 4-
   Procedimiento almacenado que muestre cuantos numeros1 y numeros2 son
   mayores que 50.
*/
-- crea tabla para rellenar con los procedimientos
CREATE TABLE datos (
 datos_id INT(11) NOT NULL AUTO_INCREMENT,
 numero1 INT(11) NOT NULL,
 numero2 INT(11) NOT NULL,
 suma VARCHAR(255) DEFAULT NULL,
 resta VARCHAR(255) DEFAULT NULL,
 rango VARCHAR(5) DEFAULT NULL,
 texto1 VARCHAR(25) DEFAULT NULL,
 texto2 VARCHAR(25) DEFAULT NULL,
 junto VARCHAR(255) DEFAULT NULL,
 longitud VARCHAR(255) DEFAULT NULL,
 tipo INT(11) NOT NULL,
 numero INT(11) NOT NULL,
 PRIMARY KEY (datos_id)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Esta tabla esta para poder actualizarla con los procedimientos',
ROW_FORMAT = Compact,
TRANSACTIONAL = 0;
ALTER TABLE datos
 ADD INDEX numero2_index(numero2);
ALTER TABLE datos
 ADD INDEX numero_index(numero);
ALTER TABLE datos
 ADD INDEX tipo_index(tipo);

-- inserta datos en la tabla
INSERT INTO datos (numero1, numero2)
  VALUES (54, 64),
         (87, 15),
         (65, 2),
         (89, 32),
         (51, 65)
;

SELECT * FROM datos d;

-- crea el procedimiento ej4
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4()
  BEGIN
    DECLARE resultado int;
    DECLARE num1 int;
    DECLARE num2 int;

    SELECT COUNT(*) INTO num1 FROM datos WHERE numero1>50;
    SET num2=(SELECT COUNT(*) FROM datos WHERE numero2>50);
    SET resultado=num1+num2;

    SELECT resultado;
  END //
DELIMITER ;

CALL ej4();

/*
  Ejemplo 5-
  Procedimiento almacenado que calcule la suma y la resta de numero1 y
  numero2
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej5()
BEGIN
  UPDATE datos
  set suma=numero1+numero2,
      resta=numero1-numero2;
END //
DELIMITER ;

CALL ej5();

SELECT * FROM datos d;

/*
  Ejemplo 6-
  Procedimiento almacenado que primero ponga todos los valores de suma y resta a NULL
  y depues calcule la suma solamente si el numero1 es mayor que el numero2
  y calcule la resta de numero2-numero1 si el numero2 es mayor o
  numero1-numero2 si es mayor el numero1 
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej6()
  BEGIN
    -- campos suma y resta a null
    UPDATE datos d
      set d.suma=NULL,
          d.resta=NULL;
  
    -- suma
    UPDATE datos d
      set d.suma=d.numero1+d.numero2
      WHERE d.numero1>d.numero2;
  
    -- resta
    UPDATE datos d
      set d.resta=d.numero1-d.numero2
      WHERE d.numero1>d.numero2;
  
    UPDATE datos d
      set d.resta=d.numero2-d.numero1
      WHERE d.numero1<d.numero2;

  END //
DELIMITER ;

CALL ej6();

SELECT * FROM datos d;

/*
  Ejemplo 7-
  Procedimiento almacenado que coloque en el campo junto el texto1, texto2
*/
-- insertamos nuevos registros para poder comprobar el funcionamiento del procedimiento
INSERT INTO datos (texto1, texto2)
  VALUES ('juntas', 'podemos'),
         ('fuuuuuuusion', 'ya'),
         ('esto', 'junto'),
         ('ej', 'emplo'),
         ('unamos', 'fuerzas');

SELECT * FROM datos d;

-- crea el procedimiento ej7
DELIMITER //
CREATE OR REPLACE PROCEDURE ej7()
  BEGIN
   UPDATE datos d
    set d.junto=CONCAT_WS(' ',d.texto1, d.texto2);
  END //
DELIMITER ;

CALL ej7;

SELECT * FROM datos d;

/*
  Ejemplo 8-
  Procedimiento almacenado que coloque en el campo junto el valor NULL.
  Después debe colocar en el campo junto el texto1-texto2 si el rango es A y si es rango B
  debe colocar texto1+texto. Si el rango es distinto debe colocar texto1 nada más
*/
-- insertamos nuevos registros para poder comprobar el funcionamiento del procedimiento
INSERT INTO datos (rango)
  VALUES ('B'),
         ('A'),
         ('A'),
         ('B'),
         ('A');

SELECT * FROM datos d;

-- crea el procedimiento ej8
DELIMITER //
CREATE OR REPLACE PROCEDURE ej8()
  BEGIN
    -- campo junto a NULL
    UPDATE datos d
      set d.junto=NULL;
    
    -- operacion
    UPDATE datos d
      set d.junto=IF(d.rango='A',CONCAT(texto1,'-',texto2),IF(d.rango='B',CONCAT(texto1,'+',texto2),d.texto1));
  END //
DELIMITER ;

CALL ej8();

SELECT * FROM datos d;